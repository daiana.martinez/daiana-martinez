#!/bin/bash

#Script de tarea Programación

#Variables
newfolder="mkdir -p carpeta_test/carpeta_1"
newfile="touch file_new.txt"
move="mv file_new.txt carpeta_test/carpeta_1"
final="Se ha creado la carpeta y archivo correspondientes"

#Creación de una carpeta en la directorio /home/ del usuario
$newfolder
#Se creará un archivo que se moverá a la carpeta creada
$newfile
$move

#Finaliza script con mensaje
echo $final
