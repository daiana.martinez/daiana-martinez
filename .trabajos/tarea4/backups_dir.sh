#!/bin/bash/

#Backup de directorios de usuarios registrados
DATE=$(date '+%A_%d_%B_%Y')
DIR_HOME="mkdir -p /home/{aldana,mauro,leandro,daiana}"
DIR_BACKUP="/tmp/backups_temporales_$DATE/"
FILE_LOG_CREATE="touch /tmp/backups_temporales_$DATE/fileLOG_$DATE"
DIR_LOG="mkdir -p /tmp/bkp_logs"
#
#
#Inicio de backup
echo "Se inicia el proceso de backup de los directorios ubicadas en /home/"
#
echo ...
echo ...
#Creación de las carpetas de los usuarios en /home/
$DIR_HOME
#Creación de la carpeta en donde se alojarán los backups
mkdir -p $DIR_BACKUP
#
#Creación de la carpeta en donde se alojarán los logs de los backups generados
$DIR_LOG
#
echo "Un momento más por favor..."
#Se crea el archivo log.
$FILE_LOG_CREATE
#
echo ...
echo ...
#Comienzo de compresión de directorios
tar -czf "aldana_$DATE.tgz" --absolute-names /home/aldana | tar -czf "mauro_$DATE.tgz" --absolute-names /home/mauro | tar -czf "leandro_$DATE.tgz" --absolute-names /home/leandro | tar -czf "daiana_$DATE.tgz" --absolute-names /home/daiana
echo ...
echo "Ya casi finaliza el proceso..."
#Los archivos comprimidos son movidos a la carpeta backups_temporales_$DATE
mv aldana_$DATE.tgz $DIR_BACKUP | mv mauro_$DATE.tgz $DIR_BACKUP | mv leandro_$DATE.tgz $DIR_BACKUP | mv daiana_$DATE.tgz $DIR_BACKUP
#
echo ...
echo ...
#Finaliza proceso
echo "Los backups se han generado con éxito el día $(date +%D) a las $(date +%H)hs por $USER"
echo "Los backups se han generado con éxito el día $(date +%D) a las $(date +%H)hs por $USER" >> /tmp/fileLOG_$DATE
#
#Se redirige el archivo log generado al directorio /bkp_logs
mv /tmp/fileLOG_$DATE /tmp/bkp_logs
#
echo "Bai bai"


